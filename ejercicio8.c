#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*Prototipos de los metodos*/
char *pedirTexto();
void contarVocales(char *, int[]);
void imprimir(int[]);

/*
Metodo principal
*/
void main()
{
    char *texto;
    texto = (char *) malloc(sizeof(char));
    int num[5];
    texto=pedirTexto();
    printf("%s",texto);
    printf("\n%d",strlen(texto));
    printf("\n");
    contarVocales(texto,num);
    imprimir(num);
}

/*
Pedimos al usuario el texto, y lo almacenamos en la memoria que se reservo
*/
char *pedirTexto()
{
    char *texto;
    texto = (char *) malloc(sizeof(char));
    printf("Ingrese un texto o palabra\n");
    scanf("%s",texto);
    
    return texto;
}

/*
Procedemos a contar las vocale que hayan en el texto mayusculas o minusculas
*/
void contarVocales(char *texto, int vector[]){

    for (int i = 0; i < 5; i++)
    {
        vector[i]= 0;
    }
    

    for (int i = 0; i<= strlen(texto) ; i++)
    {
        if (texto[i] == 'a' || texto[i] == 'A')
        {
           vector[0] += 1;
        }
        else if (texto[i] == 'e' || texto[i] == 'E')
        {
            vector[1] += 1;
        }
        else if (texto[i] == 'i' || texto[i] == 'I')
        {
            vector[2] += 1;
        }
        else if (texto[i] == 'o' || texto[i] == 'O')
        {
            vector[3] += 1;
        }
        else if (texto[i] == 'u' || texto[i] == 'U')
        {
            vector[4] += 1;
        }
    }
    

}

/*
Imprime las repeticiones de cada vocal segun el texto ingresado,
que se guardaron en el vector
*/
void imprimir(int vector[])
{
    printf("La vocal a se repite %d veces\n",vector[0]);
    printf("La vocal e se repite %d veces\n",vector[1]);
    printf("La vocal i se repite %d veces\n",vector[2]);
    printf("La vocal o se repite %d veces\n",vector[3]);
    printf("La vocal u se repite %d veces\n",vector[4]);
    
}