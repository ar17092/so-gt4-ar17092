#include <stdio.h>
#include <stdlib.h>
#define tamanio 5

/*Prototipo de los metodos*/
void media(float arreglo[]);
void max_min(float arreglo[]);

/*
Metodo principal
*/
int main()
{
    float *ptrNumeros;
    /*
    Asignación de memoria dinámica
    */
    ptrNumeros = (int *)malloc(tamanio*sizeof(float));

    for (int i = 0; i < tamanio; i++)
    {
        printf("Ingrese un número\n");
        scanf("%f\n",&ptrNumeros[i]);
        //scanf("%f\n",&ptrNumeros[i]);
        //getc(stdin);
    }
    for (int i = 0; i < tamanio; i++)
    {
        printf("Posicion [%d] = %.2f\n",i+1,ptrNumeros[i]);
    }
    
    media(ptrNumeros);
    max_min(ptrNumeros);
    return 0;
}

/*
Calcula la media de los numeros
*/
void media(float arreglo[]){
    float suma=0;
    float media;
    printf("\nCalculando la media\n");
    for (int i = 0; i < tamanio; i++)
    {
     suma= suma + arreglo[i];
    }
    media= suma/tamanio;
    printf("La media de los numeros es = %.2f\n",media);

}

/*
Metodo que identifica los valores maximos y minimo del arreglo
*/
void max_min(float arreglo[])
{
    float maximo=0;
    float minimo=0;
    printf("\nCalculando el máximo numero\n");
    for (int i = tamanio -1; i >0; i--)
    {
        for (int j = 0; j < i; j++)
        {
            if (arreglo[j]>maximo)
            {
                maximo = arreglo[j];
            }
            else if (arreglo[j]<maximo)
            {
                minimo=arreglo[i];
            }
            else if (arreglo[i]<minimo)
            {
                minimo = arreglo[i];
            }
            
            
            
        }
        
    }
    
    printf("El maximo numero del arreglo es: %.2f \n",maximo);
    printf("El minimo numero del arreglo es: %.2f \n",minimo);
}
