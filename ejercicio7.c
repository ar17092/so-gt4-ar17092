#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Prototipos*/
void imprimir(double coeficientes[], int grado);
void llenarCoeficientes(double coeficientes[], int grado);
void evaluarPol(double coeficientes[], int grado,double x);

/*
Metodo principal
*/
int main()
{   
    int grado;
    double x;
    printf("Ingrese el grado del polinomio\n");
    scanf("%d",&grado);
    double *coeficientes;
    /*Reservando memoria dinámica*/
    coeficientes= (double *)malloc((grado+1)* sizeof(double));
    llenarCoeficientes(coeficientes,grado);
    printf("Ingrese el valor a evaluar en el polinomio\n");
    scanf("%lf",&x);
    imprimir(coeficientes,grado);
    evaluarPol(coeficientes,grado,x);

    

    return 0;
}

/*
Imprime el polinomio
*/
void imprimir(double coeficientes[], int grado){
    printf("P(x) = ");
    for (int i = grado; i >= 0; i--)
    {
        if (i==0)
        {
            printf(" %.0f",coeficientes[i]);
        }
        else if (i>1)
        {
            printf(" %.0fx^%d +",coeficientes[i],i);
        }
        else if (i == 1)
        {
            printf(" %.0fx +",coeficientes[i]);
        }
    }
    
}

/*
metodo para almacenar los coeficientes de cada variable, en el arreglo
*/
void llenarCoeficientes(double coeficientes[], int grado){
    for (int i = 0; i <= grado; i++)
    {
        if (i==0)
        {
            printf("Ingrese el valor independiente\n");
            scanf("%lf",&coeficientes[i]);
        }
        else if (i>0)
        {
            printf("Ingrese el coeficiente x^%d\n",i);
            scanf("%lf",&coeficientes[i]);
        }
    }

}

/*
Evalua el punto que se da a 'x' en el polinomio
*/
void evaluarPol(double coeficientes[], int grado,double x){
    double fx=0;
    double xp;

    for (int i = grado; i >= 0; i--)
    {
        xp= pow(x, i);
        fx= fx + (coeficientes[i])*(xp);
    }
    printf("\nP(%.2f)= %.2f\n",x,fx);

}