#include <stdio.h>
#include <string.h>

/*
Estructura empleado
*/
typedef struct 
{
    char nombre[40];
    int dui;
    float sueldo;
} empleado;

/*
Prototipos de métodos
*/
void llenarVector(empleado arreglo[],int numEmpleados);
void imprimir(empleado arreglo[], int numEmpleados);
void ordenarNombre(empleado arreglo[],int numEmpleados);
void ordenarSueldo(empleado arreglo[], int numEmpleados);
float mediaSueldo(empleado arreglo[],int numEmpleados);

/*
Metodo principal
*/
int main(int argc, char const *argv[])
{
    
empleado empleados[1000];
int numEmpleados;
printf("Ingrese el número de empleados para llenar sus respectivos registros\n");
scanf("%d",&numEmpleados);
llenarVector(empleados, numEmpleados);
printf("Desordenados\n");
imprimir(empleados,numEmpleados);
printf("\nOrdenados por nombre\n");
ordenarNombre(empleados,numEmpleados);
imprimir(empleados,numEmpleados);
printf("\nOrdenados por sueldo de mayor a menor\n");
ordenarSueldo(empleados,numEmpleados);
imprimir(empleados,numEmpleados);
float promedio = mediaSueldo(empleados,numEmpleados);
printf("El promedio de los sueldos es de: $%.2f",promedio);

    return 0;
}

/*
Metodo que llena los campos del vector
*/
void llenarVector(empleado arreglo[],int numEmpleados)
{

    for (int i = 0; i < numEmpleados; i++)
    {
        printf("Ingrese el nombre del empleado\n");
        scanf("%s",arreglo[i].nombre);
        printf("Ingrese el DUI del empleado \n");
        scanf("%d",&arreglo[i].dui);
        printf("Ingrese el sueldo para el empleado\n");
        scanf("%f",&arreglo[i].sueldo);
    }
    

}


/*
Imprime el total de registros del vector
*/
void imprimir(empleado arreglo[], int numEmpleados)
{
    for (int i = 0; i < numEmpleados; i++)
    {
        printf("[Nombre: %s , DUI: %d , Sueldo: $%.2f]\n",arreglo[i].nombre,arreglo[i].dui,arreglo[i].sueldo);
    }
    

}

/*
Ordena las posiciones del vector según el nombre
*/
void ordenarNombre(empleado arreglo[],int numEmpleados)
{
   empleado almacenar;
    for (int i = numEmpleados-1; i > 0; i--)
    {
        for (int j = 0; j < i; j++)
        {
            //char [20]=arreglo
            if (strcmp(arreglo[i].nombre,arreglo[j].nombre) < 0)
            {
                almacenar = arreglo[i];
                arreglo[i]= arreglo[j];
                arreglo[j]=almacenar;
            }
            
        }
        
    }
}

/*
Ordena el vector segun el sueldo, de mayor a menor
*/
void ordenarSueldo(empleado arreglo[], int numEmpleados)
{

        empleado almacenar;
        for (int i = numEmpleados-1; i >0; i--)
        {
            for (int j = 0; j <i; j++)
            {
                if(arreglo[i].sueldo > arreglo[j].sueldo)
                {
                    almacenar = arreglo[i];
                    arreglo[i]= arreglo[j];
                    arreglo[j]=almacenar;
                }
            }
            
        }
        

}

/*
Calcula el promedio de los sueldo, y devuelve el promedio calculado
*/
float mediaSueldo(empleado arreglo[],int numEmpleados)
{
    float suma =0;
    float media;
    
    for (int i = 0; i < numEmpleados; i++)
    {
            suma = suma + arreglo[i].sueldo;
    }

    media = suma/numEmpleados;
    
    return media;

}