#include <stdio.h>
#include <stdlib.h>
#define tamanio 5

/*Prototipos de los metodos*/
void burbuja(int ptr[]);

/*
Metodo principal
*/
int main()
{

    int *ptrNumero;
    //Asignación de memoria dinámica, con el método malloc
    ptrNumero = (int *) malloc(tamanio* sizeof(int));

    for (int i = 0; i < 5; i++)
    {
        //Hacemos cast y llenamos con numeros al azar con el método rand
        ptrNumero[i] = (int)(rand()%101);

        printf("Pos [%d] = %d \n",i+1,ptrNumero[i]);
        
    }
    
    burbuja(ptrNumero);
    
    return 0;
}


/*
Metodo para ordenar de mayor a menor
*/
void burbuja(int ptr[])
{
   int almacenar;
    for (int i = tamanio-1; i > 0; i--)
    {
        for (int j = 0; j < i; j++)
        {
            if (ptr[j]<ptr[j+1])
            {
                almacenar = ptr[j];
                ptr[j] = ptr[j+1];
                ptr[j+1] = almacenar;
            }
            
        }
        
    }
    printf("Arreglo de numeros ordenado de mayor a menor\n");

    for (int i = 0; i < tamanio; i++)
    {
        printf("ptrNumero [%d] = %d\n",i+1, ptr[i]);
    }
    
}

