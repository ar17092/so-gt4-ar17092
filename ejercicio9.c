#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
Estructura de palabra
*/
typedef struct {
    char palabra[20];
}Palabra;

/*Prototipo de los metodos*/
void llenar(Palabra arreglo[]);
void imprimir(Palabra arreglo[]);
void ordenar(Palabra arreglo[]);

int main(int argc, char const *argv[])
{
    Palabra palabras[4];
    printf("Ingrese 4 palabras\n");
    llenar(palabras);

    printf("Palabras desordenadas\n");
    imprimir(palabras);

    printf("Palabras ordenadas\n");
    ordenar(palabras);
    imprimir(palabras);
    return 0;
}

/*
Metodo para ingresar las palabras
*/
void llenar(Palabra arreglo[])
{
    for (int i = 0; i < 4; i++)
    {
        printf("Ingrese la palabra#%d\n",i+1);
        scanf("%s",arreglo[i].palabra);
    }
}


/*
Imprime las palabras ingresadas por el usuario
*/
void imprimir(Palabra arreglo[])
{
    for (int i = 0; i < 4; i++)
    {
        printf("[%s]\n",arreglo[i].palabra);
    }
    
}

/*Ordena las palabras en orden alfabetico*/
void ordenar(Palabra arreglo[])
{
   Palabra almacenar;
    for (int i = 4-1; i > 0; i--)
    {
        for (int j = 0; j < i; j++)
        {
            //char [20]=arreglo
            if (strcmp(arreglo[i].palabra,arreglo[j].palabra) < 0)
            {
                almacenar = arreglo[i];
                arreglo[i]= arreglo[j];
                arreglo[j]=almacenar;
            }
            
        }
        
    }
}
